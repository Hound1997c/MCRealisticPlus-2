package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

public class CooldownFixTask implements Runnable {

    private final MCRealistic plugin;

    public CooldownFixTask(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.hasMetadata("digging") && player.getTargetBlock(null, 6).getType() == Material.AIR) {
                player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                player.removeMetadata("digging", plugin);
            }
        }
    }
}
