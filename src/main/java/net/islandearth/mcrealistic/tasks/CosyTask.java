package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Furnace;
import org.bukkit.configuration.Configuration;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class CosyTask implements Runnable {

    private final MCRealistic plugin;
    private final List<World> worlds;
    private Configuration config;
    private ConfigurationSection section;

    private int intervalCounter = 0;

    public CosyTask(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }


    @Override
    public void run() {
        if (plugin.getConfig().getBoolean("Server.Player.Cosy.DisplayMessage")) {
            config = plugin.getConfig();
            section = config.getConfigurationSection("Server.Player.Cosy");
            if (++intervalCounter != section.getInt("CallInterval")) {
                return;
            }
            intervalCounter = 0;
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (worlds.contains(player.getWorld())) {
                    MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
                    if (mcRealisticPlayer == null) return;
                    mcRealisticPlayer.setNearWarmthSource(false);

                    int furnaceRadius = section.getInt("FurnaceRadius");
                    double torchRadius = section.getDouble("TorchRadius");
                    double inclementWeatherHeatReduction = section.getDouble("InclementWeatherHeatReduction");
                    int highestBlockAtPlayer = player.getWorld().getHighestBlockYAt(player.getLocation());

                    if (player.getWorld().hasStorm() && player.getLocation().getY() >= highestBlockAtPlayer) {
                        furnaceRadius *= inclementWeatherHeatReduction;
                        torchRadius *= inclementWeatherHeatReduction;
                    }

                    for (Block block :
                            Utils.getNearbyBlocks(player.getLocation(), furnaceRadius)) {

                        Material blockType = block.getType();
                        if ((blockType.equals(Material.FIRE)
                                || blockType.equals(Material.FURNACE))
                                || blockType.equals(Material.LAVA)) {

                            if (section.getBoolean("FurnaceMustBeLit")
                                    && blockType.equals(Material.FURNACE)
                                    && ((Furnace) block.getState()).getBurnTime() == 0) {
                                break;
                            }

                            playerIsCosy(player, section.getInt("FurnaceDuration"));
                            mcRealisticPlayer.setNearWarmthSource(true);
                            break;

                        } else if (player.getLocation().distance(block.getLocation()) <= torchRadius
                                && (blockType.equals(Material.TORCH) || blockType.equals(Material.WALL_TORCH))) {
                            playerIsCosy(player, section.getInt("TorchDuration"));
                            mcRealisticPlayer.setNearWarmthSource(true);
                        }
                    }
                }
            }
        }
    }

    private void playerIsCosy(Player player, int duration) {
        if (plugin.getColds().contains(player.getUniqueId()) || plugin.getDiseases().contains(player.getUniqueId()))
            return;

        int timeDuration = duration * 60;
        Translations.COSY.send(player);
        int foodLevel = player.getFoodLevel();
        if (section.getInt("FoodLevelForRegeneration") <= foodLevel) {
            int foodDepletion = section.getInt("FoodUseToRecoverHealth");

            player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, timeDuration, 0));
            if (player.getHealth() < 20) {

                PlayerFoodDepletionTask depletionTask = new PlayerFoodDepletionTask(player,
                        foodDepletion * duration);
                int id = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin,
                        depletionTask, 0, timeDuration / (2 * foodDepletion));
                depletionTask.setTaskId(id);
            }
        }
    }
}

