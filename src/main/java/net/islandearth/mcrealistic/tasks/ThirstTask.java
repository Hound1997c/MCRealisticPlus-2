package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.List;

public class ThirstTask implements Runnable {

    private final MCRealistic plugin;
    private final List<World> worlds;

    public ThirstTask(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (worlds.contains(player.getWorld())) {
                if (player.getGameMode() == GameMode.SURVIVAL) {
                    MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
                    if (mcRealisticPlayer == null) return;
                    int currentThirst = mcRealisticPlayer.getThirst();
                    int maxThirst = getConfig().getInt("Server.Player.Max Thirst");
                    if (currentThirst <= maxThirst && currentThirst > 0) {
                        int thirstAddition = Math.min((currentThirst + 10), maxThirst);
                        mcRealisticPlayer.setThirst(thirstAddition);
                        Translations.GETTING_THIRSTY.send(player);
                        player.addPotionEffect((new PotionEffect(PotionEffectType.CONFUSION, 10, 10)));
                    }

                    if (currentThirst >= getConfig().getInt("Server.Player.Max Thirst")) {
                        Translations.REALLY_THIRSTY.send(player);
                        player.damage(3.0);
                        player.addPotionEffect((new PotionEffect(PotionEffectType.CONFUSION, 10, 10)));
                        player.addPotionEffect((new PotionEffect(PotionEffectType.BLINDNESS, 10, 10)));
                        mcRealisticPlayer.setFatigue(mcRealisticPlayer.getFatigue() + 20);
                    }

                    if (currentThirst != 0) continue;
                    mcRealisticPlayer.setThirst(currentThirst + 5);
                    Translations.LITTLE_THIRSTY.send(player);
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
