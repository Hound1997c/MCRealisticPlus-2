package net.islandearth.mcrealistic.tasks;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.Random;

public class WindTask implements Runnable {

    private final MCRealistic plugin;

    public WindTask(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (playWind(player)) {
                float windStrength = (float) (((player.getLocation().getY() - 150) / (256 - 150)) * 100);
                player.playSound(player.getLocation(), Sound.ITEM_ELYTRA_FLYING, windStrength, 1.0F);
            }
        }
    }

    private boolean playWind(Player player) {
        if (!Utils.isWorldEnabled(player.getWorld())) return false;
        if (player.getLocation().getY() < 150) return false;
        if (player.getWorld().getHighestBlockYAt(player.getLocation()) > player.getLocation().getY()) return false;
        return new Random().nextInt(50) < 25;
    }
}
