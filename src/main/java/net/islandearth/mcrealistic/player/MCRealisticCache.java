package net.islandearth.mcrealistic.player;

import org.bukkit.entity.Player;
import org.jetbrains.annotations.Nullable;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class MCRealisticCache {

    private final Map<UUID, MCRealisticPlayer> players = new ConcurrentHashMap<>();

    public void addPlayer(Player player) {
        players.put(player.getUniqueId(), new MCRealisticPlayer(player));
    }

    public void addPlayer(MCRealisticPlayer player) {
        players.put(player.getBukkitPlayer().getUniqueId(), player);
    }

    public void removePlayer(Player player) {
        players.remove(player.getUniqueId());
    }

    @Nullable
    public MCRealisticPlayer getPlayer(Player player) {
        return players.get(player.getUniqueId());
    }
}
