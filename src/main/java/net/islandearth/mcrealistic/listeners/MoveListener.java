package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class MoveListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;
    private final List<UUID> burn;

    public MoveListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        this.burn = plugin.getBurning();
    }

    @EventHandler
    public void onPlayerMove(PlayerMoveEvent pme) {
        Player player = pme.getPlayer();
        if (worlds.contains(player.getWorld())) {
            int max = 20;
            int min = 1;
            int randomNum = ThreadLocalRandom.current().nextInt((max - min) + 1) + min;
            Block blockUnder = player.getLocation().clone().subtract(0, 0.9, 0).getBlock();
            Location blockAbove = player.getLocation();
            blockAbove.setY(blockAbove.getY() + 10);
            if (player.getGameMode() == GameMode.SURVIVAL) {
                boolean inRegion = plugin.getManagers().getIntegrationManager().isInRegion(player.getLocation());
                if (player.getVelocity().getY() == -0.0784000015258789 // Player is on ground
                        && getConfig().getBoolean("Server.Player.Trail.Enabled")
                        && !inRegion
                        && pme.getTo() != null // Why is this marked nullable?
                        && pme.getTo().distance(pme.getFrom()) > 0.0) {
                    List<String> options = null;
                    if (randomNum == 3 && blockUnder.getType() == Material.GRASS_BLOCK) {
                        options = getConfig().getStringList("Server.Player.Trail.Grass_Blocks");
                    } else if (randomNum == 1 && blockUnder.getType() == Material.SAND) {
                        options = getConfig().getStringList("Server.Player.Trail.Sand_Blocks");
                    } else if (randomNum == 7 && (blockUnder.getType() == Material.DIRT || blockUnder.getType() == Material.COARSE_DIRT)) {
                        options = getConfig().getStringList("Server.Player.Trail.Dirt_Blocks");
                    } else if (randomNum == 13 && blockUnder.getType() == Material.GRASS_PATH) {
                        options = getConfig().getStringList("Server.Player.Trail.Path_Blocks");
                    }

                    final List<String> finalOptions = options;
                    if (finalOptions != null && !finalOptions.isEmpty()) {
                        Bukkit.getScheduler().runTaskLater(plugin, () -> {
                            blockUnder.setType(Material.valueOf(finalOptions.get(ThreadLocalRandom.current().nextInt(finalOptions.size()))));
                        }, 10L);
                    }
                }

                if (player.getLocation().getBlock().getType() == Material.TORCH) {
                    if (!burn.contains(player.getUniqueId())) {
                        burn.add(player.getUniqueId());
                    }
                } else if (burn.contains(player.getUniqueId())) {
                    player.setFireTicks(0);
                    burn.remove(player.getUniqueId());
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
