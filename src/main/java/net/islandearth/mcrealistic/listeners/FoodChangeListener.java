/*******************************************************************************
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 ******************************************************************************/
package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.Utils;
import org.bukkit.GameMode;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class FoodChangeListener implements Listener {

    private final MCRealistic plugin;

    public FoodChangeListener(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent flce) {
        if (flce.getEntity() instanceof Player) {
            Player player = (Player) flce.getEntity();
            MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
            if (mcRealisticPlayer == null) return;
            if (!Utils.isWorldEnabled(player.getWorld())) return;

            if (player.getGameMode() != GameMode.CREATIVE && player.getGameMode() != GameMode.SPECTATOR) {
                if (getConfig().getBoolean("Server.Player.DisplayHungerMessage") && player.getFoodLevel() < 6) {
                    Translations.HUNGRY.send(player);
                    int currentFatigue = mcRealisticPlayer.getFatigue();
                    mcRealisticPlayer.setFatigue(++currentFatigue);
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
