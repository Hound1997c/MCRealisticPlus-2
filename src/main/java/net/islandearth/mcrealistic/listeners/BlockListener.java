package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gamemode.CustomGameMode;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.BlockUtils;
import net.islandearth.mcrealistic.utils.ExtraTags;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.data.BlockData;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockDamageEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class BlockListener implements Listener {

    private MCRealistic plugin;
    private List<World> worlds;
    private List<Material> beds;
    private Set<Material> logs;
    private Set<Material> leaves;
    private List<Material> ignore;
    private List<Material> buildingIgnore;

    public BlockListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        this.beds = Arrays.asList(Material.BLACK_BED,
                Material.BLUE_BED,
                Material.BROWN_BED,
                Material.CYAN_BED,
                Material.GRAY_BED,
                Material.GREEN_BED,
                Material.LIGHT_BLUE_BED,
                Material.LIGHT_GRAY_BED,
                Material.LIME_BED,
                Material.MAGENTA_BED,
                Material.ORANGE_BED,
                Material.PINK_BED,
                Material.PURPLE_BED,
                Material.RED_BED,
                Material.WHITE_BED,
                Material.YELLOW_BED);
        this.logs = Tag.LOGS.getValues();
        this.leaves = Tag.LEAVES.getValues();
        this.ignore = Arrays.asList(Material.GRASS,
                Material.TALL_GRASS,
                Material.SEAGRASS,
                Material.TALL_SEAGRASS,
                Material.FLOWER_POT,
                Material.SUNFLOWER,
                Material.CHORUS_FLOWER,
                Material.OXEYE_DAISY,
                Material.DEAD_BUSH,
                Material.FERN,
                Material.DANDELION,
                Material.POPPY,
                Material.BLUE_ORCHID,
                Material.ALLIUM,
                Material.AZURE_BLUET,
                Material.RED_TULIP,
                Material.ORANGE_TULIP,
                Material.WHITE_TULIP,
                Material.PINK_TULIP,
                Material.BROWN_MUSHROOM,
                Material.RED_MUSHROOM,
                Material.END_ROD,
                Material.ROSE_BUSH,
                Material.PEONY,
                Material.LARGE_FERN,
                Material.REDSTONE,
                Material.REPEATER,
                Material.COMPARATOR,
                Material.LEVER,
                Material.SEA_PICKLE,
                Material.SUGAR_CANE,
                Material.FIRE,
                Material.WHEAT,
                Material.WHEAT_SEEDS,
                Material.CARROTS,
                Material.BEETROOT,
                Material.BEETROOT_SEEDS,
                Material.MELON,
                Material.MELON_STEM,
                Material.MELON_SEEDS,
                Material.POTATOES,
                Material.PUMPKIN,
                Material.PUMPKIN_STEM,
                Material.PUMPKIN_SEEDS);

        List<Material> buildingIgnore = new ArrayList<>();
        for (String string : plugin.getConfig().getStringList("Server.Building.Ignored_Blocks")) {
            buildingIgnore.add(Material.valueOf(string));
        }

        this.buildingIgnore = buildingIgnore;
    }

    @EventHandler
    public void onBlockPlace(BlockPlaceEvent bpe) {
        Player player = bpe.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        Block block = bpe.getBlock();
        boolean isInRegion = plugin.getManagers().getIntegrationManager().isInRegion(block.getLocation());

        if (worlds.contains(player.getWorld())
                && player.getGameMode().equals(GameMode.SURVIVAL)
                && !isInRegion) {
            if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
                if (mcRealisticPlayer.getFatigue() >= getConfig().getInt("Server.Player.Max Fatigue")
                        && !beds.contains(block.getType())) {
                    Translations.TOO_TIRED.send(player);
                    bpe.setCancelled(true);
                    return;
                }

                mcRealisticPlayer.setFatigue(mcRealisticPlayer.getFatigue() + 1);
            }

            if (getConfig().getBoolean("Server.Building.Realistic_Building")
                    && player.hasPermission("MCRealistic.gravity")) {
                Material l1 = bpe.getBlock().getLocation().subtract(1.0, 0.0, 0.0).getBlock().getType();
                Material l2 = bpe.getBlock().getLocation().subtract(0.0, 0.0, 1.0).getBlock().getType();
                Material l3 = bpe.getBlock().getLocation().add(1.0, 0.0, 0.0).getBlock().getType();
                Material l4 = bpe.getBlock().getLocation().add(0.0, 0.0, 1.0).getBlock().getType();
                Material l5 = bpe.getBlock().getLocation().add(0.0, 1.0, 0.0).getBlock().getType();
                if (Tag.FENCES.getValues().contains(block.getType())) {
                    int amount = 0;
                    if (l1 != Material.AIR) amount++;
                    if (l2 != Material.AIR) amount++;
                    if (l3 != Material.AIR) amount++;
                    if (l4 != Material.AIR) amount++;
                    if (l5 != Material.AIR) amount++;
                    if (amount < 2 && !Tag.FENCES.getValues().contains(bpe.getBlock().getLocation().clone().subtract(0, 1, 0).getBlock().getType())) {
                        Location loc = block.getLocation();
                        loc.setX(loc.getX() + 0.5);
                        loc.setZ(loc.getZ() + 0.5);
                        loc.getWorld().spawnFallingBlock(loc, block.getBlockData());
                        block.setType(Material.AIR);
                    }
                }

                if (player.getInventory().getItemInMainHand().hasItemMeta()) {
                    return;
                } else if (ignore.contains(bpe.getBlock().getType()) || buildingIgnore.contains(bpe.getBlock().getType())) {
                    return;
                } else if (bpe.getBlock().getLocation().subtract(0.0, 1.0, 0.0).getBlock().getType() != Material.AIR) {
                    return;
                } else if (logs.contains(l1) || logs.contains(l2) || logs.contains(l3) || logs.contains(l4) || logs.contains(l5)) {
                    return;
                }

                Location loc = block.getLocation();
                loc.setX(loc.getX() + 0.5);
                loc.setZ(loc.getZ() + 0.5);
                loc.getWorld().spawnFallingBlock(loc, block.getBlockData());
                block.setType(Material.AIR);
            }
        }
    }

    @EventHandler
    public void onBlockBreak(BlockBreakEvent bbe) {
        Player player = bbe.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        Block block = bbe.getBlock();
        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        boolean isInRegion = plugin.getManagers().getIntegrationManager().isInRegion(block.getLocation());

        if (worlds.contains(player.getWorld())
                && player.getGameMode() == GameMode.SURVIVAL
                && !isInRegion) {
            if (getConfig().getBoolean("Server.World.Falling_Trees")) {
                Location above = new Location(block.getWorld(), block.getLocation().getX() + 0.5, block.getLocation().getY() + 1, block.getLocation().getZ() + 0.5);

                for (int i = 0; i < 256; i++) {
                    if (logs.contains(above.getBlock().getType())) {
                        BlockData data = above.getBlock().getBlockData();
                        above.getBlock().setType(Material.AIR);
                        above.getWorld().spawnFallingBlock(above, data);
                        above.setY(above.getY() + 1);
                    } else break;
                }
            }

            if (getConfig().getBoolean("Server.Building.Realistic_Building")
                    && player.hasPermission("MCRealistic.gravity")) {
                Location above = block.getLocation().clone().add(0, 1, 0);
                if (Tag.FENCES.getValues().contains(block.getType())) {
                    for (int i = 0; i < 6; i++) {
                        if (!ignore.contains(above.getBlock().getType()) && !buildingIgnore.contains(above.getBlock().getType())) {
                            BlockData data = above.getBlock().getBlockData();
                            above.getBlock().setType(Material.AIR);
                            above.getWorld().spawnFallingBlock(above, data);
                            above.setY(above.getY() + 1);
                        } else break;
                    }
                }
            }

            if (gamemode == CustomGameMode.PRE_ADVENTURE) {
                if (!bbe.isCancelled() && leaves.contains(block.getType())) {
                    Random random = new Random();
                    int randomTreeChop = random.nextInt(2);

                    switch (randomTreeChop) {
                        case 0:
                            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                            break;
                        case 1:
                            bbe.setDropItems(false);
                            break;
                        case 2:
                            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                            player.getWorld().dropItem(block.getLocation(), new ItemStack(Material.STICK));
                            break;
                    }
                }
            }

            if (getConfig().getBoolean("Server.Player.Allow Fatigue")) {
                if (mcRealisticPlayer.getFatigue() >= getConfig().getInt("Server.Player.Max Fatigue")) {
                    Translations.TOO_TIRED.send(player);
                    bbe.setCancelled(true);
                    return;
                }

                mcRealisticPlayer.setFatigue(mcRealisticPlayer.getFatigue() + 1);
            }

            if (logs.contains(block.getType())) {
                if (!ExtraTags.AXES.getMaterials().contains(player.getInventory().getItemInMainHand().getType()) && !getConfig().getBoolean("Server.Player.Allow Chop Down Trees With Hands")) {
                    Translations.NO_HAND_CHOP.send(player);
                    bbe.setCancelled(true);
                } else if (ExtraTags.AXES.getMaterials().contains(player.getInventory().getItemInMainHand().getType())
                        && getConfig().getBoolean("Server.Player.Trees have random number of drops")) {
                    Random random = new Random();
                    int randomTreeChop = random.nextInt(2);

                    switch (randomTreeChop) {
                        case 0:
                            player.getWorld().dropItem(block.getLocation(), block.getState().getData().toItemStack(1));
                            break;
                        case 1:
                            bbe.setDropItems(false);
                            break;
                        case 2:
                            player.getWorld().dropItem(block.getLocation(), block.getState().getData().toItemStack(2));
                            break;
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onDamage(BlockDamageEvent bde) {
        Player player = bde.getPlayer();
        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        if (gamemode == CustomGameMode.PRE_ADVENTURE
                && player.getGameMode() == GameMode.SURVIVAL) {
            Block clicked = bde.getBlock();

            if (!leaves.contains(clicked.getType())
                    && !ignore.contains(clicked.getType())) {

                if (clicked.getType() == Material.GRAVEL) {
                    if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                        if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
                    }
                } else if (!BlockUtils.isStrong(player.getInventory().getItemInMainHand(), clicked)) {
                    bde.setCancelled(true);
                    player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100000, 255, false, false));
                    player.setMetadata("digging", new FixedMetadataValue(plugin, "digging"));
                } else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                    player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                    if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
                }
            } else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
