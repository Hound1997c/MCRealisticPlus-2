package net.islandearth.mcrealistic.listeners;

import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gamemode.CustomGameMode;
import net.islandearth.mcrealistic.player.MCRealisticPlayer;
import net.islandearth.mcrealistic.translation.Translations;
import net.islandearth.mcrealistic.utils.BlockUtils;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Tag;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

public class InteractListener implements Listener {

    private final MCRealistic plugin;
    private final List<World> worlds;
    private final Set<Material> leaves;
    private final List<Material> beds;
    private final List<Material> ignore;

    public InteractListener(MCRealistic plugin) {
        this.plugin = plugin;
        this.worlds = plugin.getWorlds();
        this.beds = Arrays.asList(Material.BLACK_BED,
                Material.BLUE_BED,
                Material.BROWN_BED,
                Material.CYAN_BED,
                Material.GRAY_BED,
                Material.GREEN_BED,
                Material.LIGHT_BLUE_BED,
                Material.LIGHT_GRAY_BED,
                Material.LIME_BED,
                Material.MAGENTA_BED,
                Material.ORANGE_BED,
                Material.PINK_BED,
                Material.PURPLE_BED,
                Material.RED_BED,
                Material.WHITE_BED,
                Material.YELLOW_BED);
        this.leaves = Tag.LEAVES.getValues();
        this.ignore = Arrays.asList(Material.GRASS,
                Material.TALL_GRASS,
                Material.SEAGRASS,
                Material.TALL_SEAGRASS,
                Material.FLOWER_POT,
                Material.SUNFLOWER,
                Material.CHORUS_FLOWER,
                Material.OXEYE_DAISY,
                Material.DEAD_BUSH,
                Material.FERN,
                Material.DANDELION,
                Material.POPPY,
                Material.BLUE_ORCHID,
                Material.ALLIUM,
                Material.AZURE_BLUET,
                Material.RED_TULIP,
                Material.ORANGE_TULIP,
                Material.WHITE_TULIP,
                Material.PINK_TULIP,
                Material.BROWN_MUSHROOM,
                Material.RED_MUSHROOM,
                Material.END_ROD,
                Material.ROSE_BUSH,
                Material.PEONY,
                Material.LARGE_FERN,
                Material.REDSTONE,
                Material.REPEATER,
                Material.COMPARATOR,
                Material.LEVER,
                Material.SEA_PICKLE,
                Material.SUGAR_CANE,
                Material.FIRE,
                Material.WHEAT,
                Material.WHEAT_SEEDS,
                Material.CARROTS,
                Material.BEETROOT,
                Material.BEETROOT_SEEDS,
                Material.MELON,
                Material.MELON_STEM,
                Material.MELON_SEEDS,
                Material.POTATOES,
                Material.PUMPKIN,
                Material.PUMPKIN_STEM,
                Material.PUMPKIN_SEEDS);
    }

    @EventHandler(priority = EventPriority.LOW)
    public void onInteract(PlayerInteractEvent pie) {
        Player player = pie.getPlayer();
        MCRealisticPlayer mcRealisticPlayer = plugin.getCache().getPlayer(player);
        if (mcRealisticPlayer == null) return;
        CustomGameMode gamemode = CustomGameMode.valueOf(getConfig().getString("Server.GameMode.Type").toUpperCase());
        if (worlds.contains(player.getWorld())) {
            if (gamemode == CustomGameMode.PRE_ADVENTURE
                    && pie.getAction() == Action.LEFT_CLICK_BLOCK
                    && player.getGameMode() == GameMode.SURVIVAL) {
                Block clicked = pie.getClickedBlock();
                if (!leaves.contains(clicked.getType()) && !ignore.contains(clicked.getType())) {

                    if (clicked.getType() == Material.GRAVEL) {
                        if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                            player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                            if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
                        }
                    } else if (!BlockUtils.isStrong(player.getInventory().getItemInMainHand(), clicked)) {
                        pie.setCancelled(true);
                        player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW_DIGGING, 100000, 255, false, false));
                        player.setMetadata("digging", new FixedMetadataValue(plugin, "digging"));
                    } else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                        player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                        if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
                    }
                } else if (player.hasPotionEffect(PotionEffectType.SLOW_DIGGING)) {
                    player.removePotionEffect(PotionEffectType.SLOW_DIGGING);
                    if (player.hasMetadata("digging")) player.removeMetadata("digging", plugin);
                }
            }

            if (pie.getAction() == Action.RIGHT_CLICK_AIR || pie.getAction() == Action.RIGHT_CLICK_BLOCK) {
                // Waypoints
                if (player.getInventory().getItemInMainHand().getType() == Material.COMPASS) {
                    player.setCompassTarget(player.getLocation());
                    mcRealisticPlayer.setWaypointX(player.getLocation().getBlockX());
                    mcRealisticPlayer.setWaypointY(player.getLocation().getBlockY());
                    mcRealisticPlayer.setWaypointZ(player.getLocation().getBlockZ());
                    Translations.WAYPOINT_SET.send(player);
                }

                // Check for broken bones
                if (player.getInventory().getItemInMainHand().getType() == Material.PAPER &&
                        player.getInventory().getItemInMainHand().getItemMeta() != null
                        && player.getInventory().getItemInMainHand().getItemMeta().getDisplayName().equals(ChatColor.DARK_AQUA + "Bandage")
                        && mcRealisticPlayer.hasBrokenBones()) {
                    mcRealisticPlayer.setHasBrokenBones(false);
                    Translations.USED_BANDAGE.send(player);
                    player.getInventory().removeItem(new ItemStack(player.getInventory().getItemInMainHand()));
                    player.updateInventory();
                    player.removePotionEffect(PotionEffectType.SLOW);
                } else if (pie.getAction().equals(Action.RIGHT_CLICK_BLOCK) && pie.getClickedBlock() != null && !pie.getClickedBlock().getType().equals(Material.AIR)) {
                    if (beds.contains(pie.getClickedBlock().getType())) {
                        // Check for fatigue
                        if (getConfig().getBoolean("Server.Player.Allow Fatigue") && mcRealisticPlayer.getFatigue() != 0) {
                            mcRealisticPlayer.setFatigue(0);
                            Translations.NOT_TIRED.send(player);
                        }
                    }
                }
            }
        }
    }

    private FileConfiguration getConfig() {
        return plugin.getConfig();
    }
}
