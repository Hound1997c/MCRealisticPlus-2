package net.islandearth.mcrealistic.api;

public final class MCRealisticAPI {

    private MCRealisticAPI() {}

    private static IMCRealisticAPI api;

    public static IMCRealisticAPI getAPI() {
        return api;
    }

    public static void setAPI(IMCRealisticAPI api) {
        if (MCRealisticAPI.api != null && api != null) throw new IllegalStateException("API already set");
        MCRealisticAPI.api = api;
    }
}
