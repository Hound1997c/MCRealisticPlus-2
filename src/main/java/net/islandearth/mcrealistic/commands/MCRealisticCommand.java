package net.islandearth.mcrealistic.commands;

import co.aikar.commands.BaseCommand;
import co.aikar.commands.CommandHelp;
import co.aikar.commands.annotation.CommandAlias;
import co.aikar.commands.annotation.CommandCompletion;
import co.aikar.commands.annotation.CommandPermission;
import co.aikar.commands.annotation.Default;
import co.aikar.commands.annotation.Optional;
import co.aikar.commands.annotation.Subcommand;
import co.aikar.commands.bukkit.contexts.OnlinePlayer;
import net.islandearth.mcrealistic.MCRealistic;
import net.islandearth.mcrealistic.gui.ItemGUI;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionData;
import org.bukkit.potion.PotionType;

import java.util.Arrays;
import java.util.Collections;

@CommandAlias("mcrealistic|mcr")
public class MCRealisticCommand extends BaseCommand {

    private final MCRealistic plugin;

    public MCRealisticCommand(MCRealistic plugin) {
        this.plugin = plugin;
    }

    @Default
    public void onDefault(CommandHelp help) {
        help.showHelp();
    }

    @Subcommand("reload")
    @CommandPermission("mcr.reload")
    public void onReload(CommandSender sender) {
        sender.sendMessage(ChatColor.GREEN + "Reloading...");
        plugin.reloadConfig();
        plugin.saveConfig();
        sender.sendMessage(ChatColor.GREEN + "Done!");
    }

    @Subcommand("items")
    @CommandPermission("mcr.items")
    public void onItems(Player player) {
        new ItemGUI(plugin, player).open();
    }

    @Subcommand("item")
    @CommandPermission("mcr.items")
    @CommandCompletion("@items @range:20 @players")
    public void onItem(CommandSender sender, String item, int amount, @Optional OnlinePlayer target) {
        if (target == null && !(sender instanceof Player)) return;
        Player player = target == null ? (Player) sender : target.getPlayer();
        switch (item.toLowerCase()) {
            case "medicine":
                ItemStack medicine = new ItemStack(Material.POTION, amount);
                ItemMeta medicinemeta = medicine.getItemMeta();
                medicinemeta.setDisplayName(ChatColor.GREEN + "Medicine");
                medicinemeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to help fight your cold/disease!"));
                medicine.setItemMeta(medicinemeta);
                player.getInventory().addItem(medicine);
                break;
            case "bandage":
                ItemStack bandage = new ItemStack(Material.PAPER, amount);
                ItemMeta bm = bandage.getItemMeta();
                bm.setDisplayName(ChatColor.DARK_AQUA + "Bandage");
                bandage.setItemMeta(bm);
                player.getInventory().addItem(bandage);
                break;
            case "chocolatemilk":
                ItemStack chocolatemilk = new ItemStack(Material.MILK_BUCKET, amount);
                ItemMeta chocolatemilkmeta = chocolatemilk.getItemMeta();
                chocolatemilkmeta.setDisplayName(ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "Chocolate Milk");
                chocolatemilkmeta.setLore(Arrays.asList(ChatColor.WHITE + "Drink to gain Speed II."));
                chocolatemilk.setItemMeta(chocolatemilkmeta);
                player.getInventory().addItem(chocolatemilk);
                break;
            case "purifiedwater":
                ItemStack water = new ItemStack(Material.POTION, amount);
                PotionMeta wm = (PotionMeta) water.getItemMeta();
                wm.setBasePotionData(new PotionData(PotionType.WATER));
                wm.setLore(Collections.singletonList(ChatColor.GRAY + "Purified"));
                water.setItemMeta(wm);
                player.getInventory().addItem(water);
                break;
            default:
                sender.sendMessage(ChatColor.RED + "Item not found.");
                break;
        }
    }
}
